Name:           tpm2-abrmd-selinux
Version:        2.3.1
Release:        1
Summary:        SELinux policies for tpm2-abrmd
License:        BSD
URL:            https://github.com/tpm2-software/tpm2-abrmd
Source0:        https://github.com/tpm2-software/tpm2-abrmd/archive/%{version}/tpm2-abrmd-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  selinux-policy-devel pkgconfig(systemd)
Requires:       selinux-policy >= 0.0.1
Requires(post): selinux-policy-base >= 0.0.1
Requires(post): libselinux-utils
Requires(post): policycoreutils
Requires(post): policycoreutils-python-utils

%description
SELinux policies for tpm2-abrmd.

%prep
%autosetup -n tpm2-abrmd-%{version} -p1

%build
pushd selinux
make %{?_smp_mflags} TARGET="tabrmd" SHARE="%{_datadir}"
popd

%install
pushd selinux
install -d %{buildroot}%{_datadir}/selinux/packages
install -d -p %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -p -m 644 tabrmd.if %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -m 0644 tabrmd.pp.bz2 %{buildroot}%{_datadir}/selinux/packages
popd

%pre
%selinux_relabel_pre -s targeted

%post
%selinux_modules_install -s targeted %{_datadir}/selinux/packages/tabrmd.pp.bz2

%postun
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s targeted tabrmd
fi

%posttrans
%selinux_relabel_post -s targeted

%files
%defattr(-,root,root)
%license LICENSE
%{_datadir}/selinux/devel/include/contrib/tabrmd.if
%{_datadir}/selinux/packages/tabrmd.pp.bz2

%changelog
* Thu Aug 20 2020 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 2.3.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.3.1

* Fri Apr 24 2020 wanghongzhe<wanghongzhe@huawei.com> - 2.2.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to 2.2.0

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-3
- Package init
